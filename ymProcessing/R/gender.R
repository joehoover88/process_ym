#' gender_process
#' 
#' This recodes the variable sex into a variable called gender, where Female = 0 and Male = 1.
#' @param x Dataframe containing the variable 'sex' 
#' @param dropNA Boolean indicating whether to drop cases without a valid gender level.

#' @keywords mfq, rename
#' @export
#' @examples 
#' gender_process(x)

gender_process <- function(x, dropNA = FALSE){
  print('Recoding gender')
  
  print('Levels detected:')
  
  print(unique(x$sex))
  
  x$gender <- dplyr::recode_factor(x$sex, 
                            'M' = 1,
                            'F' = 0,
                            'male' = 1,
                            'female' = 0)
  
  
  if (dropNA){
    x <- tidyr::drop_na(x, gender)
    return(x)
  }
  
  return(x)
}
