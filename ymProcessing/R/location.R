
#' loc_country_process
#'
#' This function determines a user's reported country and maps it to the majorcountry variable.
#' @param x Dataframe containing the variables 'country_age', 'country_diff'.
#' @keywords mfq, location, country
#' @export
#' @examples
#' loc_country_process(x)


loc_country_process <- function(x) {

  
  x$country_age_num <- as.numeric(x$country_age)
  
  if (!('age' %in% names(x))) x <- age_process(x)
  
  i <- (x$country_age_num > 1900 & !is.na(x$country_age_num))
  
  x$country_age_num[i] <- x$age[i] - (x$registrationyear[i] - x$country_age_num[i])
  
  x$country <- x$country_current
  x$country_current = NULL
  
  x <- x[order(x$country),]
  
  i <- (x$country_age_num > 14 & !is.na(x$country_age_num) & x$country_age_num != '')
  
  x$country[i] <- x$country_diff[i]
  
  states_to_skip = c("")
  
  country.names <- c(maps::world.cities$country.etc, 'United Kingdom', 'Ontario', 'Quebec', 'Nova Scotia', 'New Brunswick', 
                     'Manitoba', 'British Columbia', 'Prince Edward Island', 
                     'Saskatchewan', 'Alberta', 'Newfoundland and Labrador')
  
  for (i in 1:length(x$country)){
    
    if (grepl(',', x$country[i])){
      if (TRUE %in% stringr::str_detect(x$country[i], state.name)){
        x$country[i] = 'USA - United States of America'
      }
      
      else if (TRUE %in% stringr::str_detect(x$country[i], country.names)){
        temp = x$country[i]
        temp = strsplit(x$country[i], ',')
        temp = temp[[1]][[length(temp[[1]])]]
        temp = trimws(temp)
        x$country[i] = temp
      }
    }
  }
  
  x$country_region <- dplyr::recode(x$country, 'Albania'='Eastern Europe', 'Algeria'='Middle East', 'Andorra'='Western Europe', 'Angola'='Africa', 'Anguilla'='Latin America',
                                    'Antigua and Barbuda'='Latin America', 'Argentina'='Latin America', 'Armenia'='Eastern Europe', 'Austria'='Western Europe', 'Azerbaijan'='Middle East', 'Bahamas'='Latin America', 'Bahrain'='Middle East', 'Bangladesh'='South Asia', 'Barbados'='Latin America', 'Belarus'='Eastern Europe', 'Belgium'='Western Europe', 'Belize'='Latin America', 'Benin'='Africa', 'Bermuda'='Latin America', 'Bhutan'='South Asia',
                                    'Bolivia'='Latin America', 'Bosnia and Herzegovina'='Eastern Europe', 'Botswana'='Africa', 'Brazil'='Latin America', 'Brunei'='Southeast Asia', 'Bulgaria'='Eastern Europe', 'Burkina Faso'='Africa', 'Burundi'='Africa', 'Cambodia'='Southeast Asia', 'Cameroon'='Africa', 'Canada'='Canada', 'Cape Verde'='Africa', 'Cayman Islands'='Latin America',
                                    'Central African Republic'='Africa', 'Chad'='Africa', 'Chile'='Latin America', 'China'='East Asia', 'Colombia'='Latin America', 'Comoros'='Africa', 'Congo, Democratic Republic'='Africa', 'Costa Rica'='Latin America',
                                    'Croatia Hrvatska,'='Eastern Europe', 'Cuba'='Latin America', 'Cyprus'='Eastern Europe', 'Czech Republic'='Eastern Europe', 'Denmark'='Western Europe', 'Djibouti'='Africa', 'Dominica'='Latin America', 'Dominican Republic'='Latin America', 'East Timor'='Southeast Asia', 'Ecuador'='Latin America', 'Egypt'='Middle East', 'El Salvador'='Latin America', 'Equatorial Guinea'='Africa',
                                    'Eritrea'='Africa', 'Estonia'='Eastern Europe', 'Ethiopia'='Africa', 'Finland'='Western Europe', 'France'='Western Europe', 'Gabon'='Africa', 'Gambia'='Africa', 'Georgia'='Eastern Europe', 'Germany'='Western Europe', 'Ghana'='Africa', 'Greece'='Western Europe', 'Grenada'='Latin America', 'Guam'='Southeast Asia', 'Guatemala'='Latin America', 'Guinea'='Africa', 'Guinea-Bissau'='Africa',
                                    'Guyana'='Latin America', 'Haiti'='Latin America', 'Honduras'='Latin America', 'Hong Kong'='East Asia', 'Hungary'='Eastern Europe', 'Iceland'='Western Europe', 'India'='South Asia', 'Indonesia'='Southeast Asia', 'Iran'='Middle East', 'Iraq'='Middle East', 'Ireland'='Western Europe', 'Italy'='Western Europe', 'Jamaica'='Latin America', 'Japan'='East Asia', 'Jordan'='Middle East', 'Kazakhstan'='Central Asia', 'Kenya'='Africa',
                                    'Korea north,'='East Asia', 'Korea south,'='East Asia', 'Kuwait'='Middle East', 'Kyrgyzstan'='Central Asia', 'Lao Peoples Democratic Republ'='Southeast Asia', 'Latvia'='Eastern Europe', 'Lebanon'='Middle East', 'Lesotho'='Africa', 'Liberia'='Africa',
                                    'Libyan Arab Jamahiriya'='Middle East', 'Liechtenstein'='Western Europe', 'Lithuania'='Eastern Europe', 'Luxembourg'='Western Europe', 'Macao'='East Asia', 'Macedonia'='Eastern Europe', 'Madagascar'='Africa', 'Malawi'='Africa', 'Malaysia'='Southeast Asia', 'Maldives'='South Asia',
                                    'Mali'='Africa', 'Malta'='Eastern Europe', 'Mauritania'='Africa', 'Mauritius'='Africa', 'Mexico'='Latin America', 'Moldova'='Eastern Europe', 'Monaco'='Western Europe', 'Mongolia'='Central Asia', 'Montenegro'='Eastern Europe', 'Morocco'='Middle East', 'Mozambique'='Africa', 'Myanmar'='Southeast Asia', 'Namibia'='Africa',
                                    'Nauru'='Middle East', 'Nepal'='South Asia', 'Netherlands'='Western Europe', 'Netherlands Antilles'='Latin America', 'New Zealand'='Australia/NZ', 'Nicaragua'='Latin America', 'Niger'='Africa', 'Nigeria'='Africa', 'Norway'='Western Europe', 'Oman'='Middle East', 'Pakistan'='Middle East',
                                    'Palestinian Territories'='Middle East', 'Panama'='Latin America', 'Papua New Guinea'='Southeast Asia', 'Paraguay'='Latin America', 'Peru'='Latin America', 'Philippines'='Southeast Asia', 'Poland'='Eastern Europe', 'Portugal'='Western Europe', 'Puerto Rico'='Latin America', 'Qatar'='Middle East', 'Romania'='Eastern Europe',
                                    'Russian Federation'='Eastern Europe', 'Rwanda'='Africa', 'Saint Kitts and Nevis'='Latin America', 'Saint Lucia'='Latin America',
                                    'Saint Vincent and the Grenadines'='Latin America', 'San Marino'='Western Europe', 'Sao Tome and Principe'='Africa', 'Saudi Arabia'='Middle East', 'Senegal'='Africa', 'Serbia and Montenegro'='Eastern Europe', 'Seychelles'='Africa', 'Sierra Leone'='Africa', 'Singapore'='Southeast Asia',
                                    'Slovakia'='Eastern Europe', 'Slovenia'='Eastern Europe', 'Somalia'='Middle East', 'Spain'='Western Europe',
                                    'Sri Lanka'='South Asia', 'Sudan'='Africa', 'Suriname'='Latin America', 'Swaziland'='Africa', 'Sweden'='Western Europe', 'Switzerland'='Western Europe', 'Syria'='Middle East', 'Taiwan'='East Asia', 'Tajikistan'='Central Asia', 'Tanzania'='Africa', 'Thailand'='Southeast Asia', 'Togo'='Africa', 'Trinidad and Tobago'='Latin America', 'Tunisia'='Middle East', 'Turkey'='Middle East',
                                    'Turkmenistan'='Central Asia', 'Turks and Caicos Islands'='Latin America', 'Uganda'='Africa', 'Ukraine'='Eastern Europe', 'United Arab Emirates'='Middle East', 'United Kingdom'='UK', 'Uruguay'='Latin America', 'USA - United States of America'='USA', 'Uzbekistan'='Central Asia', 'Vatican City'='Western Europe', 'Venezuela'='Latin America', 'Vietnam'='Southeast Asia',
                                    'Virgin Islands US,'='Latin America', 'Yemen'='Middle East', 'Australia' = 'Australia/NZ', ' Australia' = 'Australia/NZ')
  

  return(x)

}


#' loc_county_process
#'
#' This function maps user provided USA zip codes to county codes (i.e. FIPS codes).
#' @param x Dataframe containing the variable 'zipcode' and 'majorcounty'.
#' @keywords mfq, location, zipcode
#' @export
#' @examples
#' loc_county_process(x)

loc_county_process <- function(x) {
  
  x$zipcode <- as.integer(x$zipcode)
  
  x$zipcode <- sprintf("%05d", x$zipcode)
  
  x$temp_id <- 1:nrow(x)
  x <- dplyr::left_join(x, zip_county, by=c('zipcode' = 'zip'))
  x <- x[!duplicated(x$temp_id),]
  
  if (!('country' %in% names(x))) x <- loc_country_process(x)
  x$county_code[(x$country_region != 'USA' | x$country == '')] <- NA
  x$zipcode[is.na(x$county_code)] <- NA
  #x$county_code <- as.factor(x$county_code)
  

  return(x)

}




#' loc_process_all
#'
#' This function implements all 'loc_' functions on the input dataframe
#' @param x Dataframe containing the variable's required for all 'loc_' functions.
#' @keywords yourmorals, location
#' @export
#' @examples
#' loc_process_all(x)

loc_process_all <- function(x) {
  x <- loc_country_process(x)
  x <- loc_county_process(x)

  return(x)

}

