#' mfq_rename
#' 
#' This function renames MFQ variables
#' @param x dataframe object containing MFQ variables
#' @keywords mfq, rename
#' @export
#' @examples 
#' mfq_rename(x)

mfq_rename <- function(x){

  x <- plyr::rename(x, c('X5f_part1_1' = 'MFQ_math',
                             'X5f_part1_2' = 'MFQ_emotionally',
                             'X5f_part1_3' = 'MFQ_weak',
                             'X5f_part1_4' = 'MFQ_cruel',
                             'X5f_part1_5' =  'MFQ_unfairly',
                             'X5f_part1_6' = 'MFQ_treated',
                             'X5f_part1_7' = 'MFQ_rights',
                             'X5f_part1_8'= 'MFQ_loyalty',  
                             'X5f_part1_9'='MFQ_betray', 
                             'X5f_part1_10'='MFQ_lovecountry', 
                             'X5f_part1_11'='MFQ_traditions',  
                             'X5f_part1_12'='MFQ_respect', 
                             'X5f_part1_13'='MFQ_chaos', 
                             'X5f_part1_14'='MFQ_disgusting',  
                             'X5f_part1_15'='MFQ_decency', 
                             'X5f_part1_16'='MFQ_god', 
                             'X5f_part2_1'= 'MFQ_good',  
                             'X5f_part2_2'='MFQ_animal', 
                             'X5f_part2_3'= 'MFQ_kill', 
                             'X5f_part2_4'= 'MFQ_compassion', 
                             'X5f_part2_5'='MFQ_justice', 
                             'X5f_part2_6'='MFQ_fairly', 
                             'X5f_part2_7'='MFQ_rich',  
                             'X5f_part2_8'= 'MFQ_team', 
                             'X5f_part2_9'='MFQ_history', 
                             'X5f_part2_10'= 'MFQ_family',  
                             'X5f_part2_11'='MFQ_sexroles', 
                             'X5f_part2_12'='MFQ_soldier', 
                             'X5f_part2_13'='MFQ_kidrespect',  
                             'X5f_part2_14'= 'MFQ_harmlessdg', 
                             'X5f_part2_15'='MFQ_unnatural', 
                             'X5f_part2_16'='MFQ_chastity')
  )
  
  return(x)
}



#' mfq_indices
#' 
#' This function generates mean indices for MFQ foundation factors. Variable names need to match those output by the mfq_rename() function.
#' @param x dataframe object containing renamed MFQ variables
#' @keywords mfq, indices
#' @export
#' @examples 
#' mfq_indices(x)

mfq_indices <- function(x) {
  
  x <- dplyr::mutate(x, MFQ_HARM_AVG = rowMeans(x[,c('MFQ_emotionally','MFQ_weak','MFQ_cruel','MFQ_animal','MFQ_kill','MFQ_compassion')], na.rm=T),
           MFQ_FAIRNESS_AVG = rowMeans(x[,c('MFQ_rights','MFQ_unfairly','MFQ_treated','MFQ_justice','MFQ_fairly','MFQ_rich')], na.rm=T),
           MFQ_INGROUP_AVG = rowMeans(x[,c('MFQ_loyalty','MFQ_betray','MFQ_lovecountry','MFQ_team','MFQ_history','MFQ_family')], na.rm=T),
           MFQ_AUTHORITY_AVG = rowMeans(x[,c('MFQ_traditions','MFQ_respect','MFQ_chaos','MFQ_sexroles','MFQ_soldier','MFQ_kidrespect')], na.rm=T),
           MFQ_PURITY_AVG = rowMeans(x[,c('MFQ_disgusting','MFQ_decency','MFQ_god','MFQ_harmlessdg','MFQ_unnatural','MFQ_chastity')], na.rm=T))
  
    return(x)
}



#' mfq_hfminiap
#' 
#' This function generates a difference variable between the individualizing foundations (Harm and Fairness) and the binding foundations (Loyalty, Authority, and Purity). Variable names need to match those output by the mfq_indices() function.
#' @param x dataframe object containing renamed MFQ variables
#' @keywords mfq, hfminusiap
#' @export
#' @examples 
#' mfq_hfminiap(x)

mfq_hfminiap <- function(x) {
  x <- dplyr::mutate(x, MFQ_HFminusIAP = rowMeans(x[,c('MFQ_HARM_AVG','MFQ_FAIRNESS_AVG')], na.rm=T) - 
             rowMeans(x[,c('MFQ_INGROUP_AVG','MFQ_AUTHORITY_AVG','MFQ_PURITY_AVG')], na.rm=T))

  return(x)
}


#' mfq_blanks
#' 
#' This function calculates the percent of MFQ items missing for each case. The result is stored in a variable calld MFQ_BLANKS_perc. Variable names need to match those output by the mfq_rename() function.
#' @param x dataframe object containing renamed MFQ variables
#' @keywords mfq, mfq_blanks
#' @export
#' @examples 
#' mfq_blanks(x)

mfq_blanks <- function(x) {
  
  MFQ_BLANKS <- apply(dplyr::select(x, MFQ_math:MFQ_chastity), MARGIN=1, 
                          FUN = function(x) length(x[is.na(x)]))
  
  x$MFQ_BLANKS_perc <- MFQ_BLANKS/32
  
  return(x)
}


#' mfq_makeNA
#' 
#' This function sets MFQ index scores as NA for given thresholds of NA items and number of times the MFQ was taken. Variable names need to match those output by the mfq_indices() function.
#' @param x dataframe object containing renamed MFQ variables
#' @param items_na_threshold cutoff for percent of missing MFQ items. MFQ foundation scores are set to NA for all cases that exceed this threshold. Defaults to .20.
#' @param numtaken_threshold cutoff for number of times MFQ was taken. MFQ foundation scores are set to NA for all cases that exceed this threshold. Defaults to 1.
#' @keywords mfq, makeNA
#' @export
#' @examples 
#' mfq_makeNA(x=x, items_na_threshold=.20, numtaken_threshold=1)

mfq_makeNA <- function(x, items_na_threshold=0.20, numtaken_threshold=1){
  makeNA <- ifelse(x$MFQ_BLANKS_perc > items_na_threshold, TRUE, 
                   ifelse(x$X5f_new2_numtaken > numtaken_threshold, TRUE, FALSE))
  
  
  x[!is.na(makeNA) & makeNA == TRUE, c('MFQ_HARM_AVG',
                                       'MFQ_FAIRNESS_AVG',
                                       'MFQ_INGROUP_AVG',
                                       'MFQ_AUTHORITY_AVG',
                                       'MFQ_PURITY_AVG')] <- NA
  
  return(x)
}


#' mfq_failed
#' 
#' This function creates a boolean TRUE/FALSE variable indicating whether a given case failed the MFQ attention check. Variable names need to match those output by the mfq_indices() function.
#' @param x dataframe object containing renamed MFQ variables
#' @keywords mfq, mfq_failed
#' @export
#' @examples 
#' mfq_failed(x)

mfq_failed <- function(x){
  x$MFQ_failed = ifelse(x$MFQ_good <= x$MFQ_math, TRUE, FALSE)
  return(x)
}



#' mfq_process
#' 
#' This function processes the MFQ variables included in the 5fnew2 MFQ data. Variables are renamed, indices are generated, missing values based on percent of items completed and number of times the MFQ was taken are calculated, and an attention check indicator is generated.
#' @param x dataframe object containing renamed MFQ variables
#' @param items_na_threshold cutoff for percent of missing MFQ items. MFQ foundation scores are set to NA for all cases that exceed this threshold. Defaults to .20.
#' @param numtaken_threshold cutoff for number of times MFQ was taken. MFQ foundation scores are set to NA for all cases that exceed this threshold. Defaults to 1.
#' @keywords mfq, mfq_failed
#' @export
#' @examples 
#' mfq_process(x)

mfq_process<- function(x, items_na_threshold=0.20, numtaken_threshold=1){
  
  x <- mfq_rename(x)
  x <- mfq_indices(x)
  x <- mfq_hfminiap(x)
  x <- mfq_blanks(x)
  x <- mfq_makeNA(x, items_na_threshold=items_na_threshold, numtaken_threshold=numtaken_threshold)
  x <- mfq_failed(x)
  
  return(x)
}





