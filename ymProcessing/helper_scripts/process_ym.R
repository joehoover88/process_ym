args <- commandArgs(trailingOnly = TRUE)

print('Loading data')

dat <- read.csv(args[1],
                header=TRUE,
                stringsAsFactors=FALSE,
                fileEncoding="latin1",
                na.strings = '""')

library(ymProcessing)
print('Processing data')
dat <- fnew2_compute_all(dat) 

print('Writing data')
write.csv(dat, file = args[2])