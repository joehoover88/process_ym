# README #

This R package was built to streamline processing raw data from yourmorals.org. Note that while this repository is public, youmorals data is not.

It is very much a work in progress. Each push will contain at least one new set of functions built to process a component of yourmorals data (e.g. MFQ items). 

## Quick Installation ##

You can use the following script to install this package: 

     install.packages("devtools")

     devtools::install_bitbucket(repo='joehoover88/process_ym',
                       subdir='ymProcessing')



## Quick Start ##

To see the list of functions available in ymProcessing, you can run the following code: 

     ls("package:ymProcessing")

In general, these functions will be named with a meaningful prefix (i.e. functions relevant to processing MFQ data are prefixed with 'mfq_'. To get more information about a specific function, you can run `?[function]` (e.g. `?mfq_process`). 

I also try to break each processing step into individual functions, which are all available. However, for each general task (e.g. processing MFQ items), there is also a comprehensive function that implements each processing function for a given domain. At the very least, I'll try to keep these comprehensive functions specified in this README.

### Processing all variables ### 

To process all variables you can use the command `fnew2_compute_all()`, which only takes one argument: a dataframe containing raw 5fnew_2 yourmorals data. 

You can also use the following script to process your data via the command line: 


```
#!R

args <- commandArgs(trailingOnly = TRUE)

print('Loading data')

dat <- read.csv(args[1],
                header=TRUE,
                stringsAsFactors=FALSE,
                fileEncoding="latin1",
                na.strings = '""')

library(ymProcessing)
print('Processing data')
dat <- fnew2_compute_all(dat) 

print('Writing data')
write.csv(dat, file = args[2])
```

To use this script:

1. Save it to an .R file called process_ym.R (make sure to note the directory)
2. Open a terminal session and run the following command: 

`Rscript path/to/process_ym.R path/to/raw_data.csv path/for/new_data.csv`

where the latter two arguments are paths to your raw data and the path to which the processed data should be written. If you store process_ym.R in an accessible location (like your home directory) and use the terminal session to navigate to the directory that contains the raw data, this command can be as simple as: 

`Rscript ~/scripts/process_ym.R 'raw_data.csv' 'processed_data.csv'`


### Processing MFQ items ###

The functions for processing MFQ items take a dataframe and implement operations on the relevant columns in that dataframe. The column names are hardcoded to match the column names in the '5fnew2' yourmorals data. Thus, if and when those names change, this code will break. 

I generally have trouble reading raw youmorals data into R, but the following code seems to work, despite the warning: 

     dat <- read.csv('../5fnew2.csv', 
                  header=TRUE,
                  stringsAsFactors=FALSE,
                  fileEncoding="latin1",
                  na.strings = '""')

To process the MFQ items, you can use the function `mfq_process`, which performs the following operations: 

* renames variables 
* creates MFQ foundation indices 
* creates a individualizing vs binding difference score
* recodes cases as missing, depending on some conditional rules

For example: 

    cleandMFQ <- mfq_process(dat)

In this case, cleanMFQ will return a dataframe that contains all of the original data (i.e. the object dat) *and* it will have the new variables created during processing appended to it. 

To save this dataframe you use `save()`, `write.csv()` or a host of other functions. 

##Contact

If you notice any bugs or have questions or requests, please contact me at jehoover@usc.edu.